A code playground that supports multiple languages. Code is sent to the server where it's (compiled, if necessary, and) executed.
Currently, there are no sandboxing measures implemented to prevent the execution of malicious code.

Uses the Ace.js editor for the front-end and Go's net/http for the back-end server.

![](http://i.imgur.com/lEuaE8F.jpg)
