package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
)

const TIMEOUT = 5

func runBin(bin string) []byte {
	sendChan := make(chan []byte, 0)
	run := exec.Command(bin)
	go func() {
		output, err := run.CombinedOutput()
		if err != nil {
			fmt.Println(err)
		}
		sendChan <- output
	}()
	select {
	case output := <-sendChan:
		return output
	case <-time.After(time.Second * TIMEOUT):
		killErr := run.Process.Kill()
		if killErr != nil {
			fmt.Println(killErr)
		}
		return []byte(fmt.Sprintf("Execution timed out as it exceeded the allowed limit of %d seconds.", TIMEOUT))
	}
}

func executeGo(src []byte) (output []byte) {
	path := os.TempDir() + string(os.PathSeparator) // TODO: should generate unique files
	srcName := "tmpgosrc.go"
	binName := "tmpgobin"
	if runtime.GOOS == "windows" { // go build -o doesn't append .exe if platform is Windows
		binName += ".exe"
	}
	ioutil.WriteFile(path+srcName, src, os.FileMode(0644))
	compile := exec.Command("go", "build", "-o", path+binName, path+srcName)
	compileOutput, err := compile.CombinedOutput()
	if err != nil {
		return compileOutput
	}
	return runBin(path + binName)
}

func executeRust(src []byte) (output []byte) {
	path := os.TempDir() + string(os.PathSeparator) // TODO: should generate unique files
	srcName := "tmprustsrc.rs"
	binName := "tmprustbin"
	ioutil.WriteFile(path+srcName, src, os.FileMode(0644))
	compile := exec.Command("rustc", path+srcName, "-o", path+binName)
	compileOutput, err := compile.CombinedOutput()
	if err != nil {
		return compileOutput
	}
	return runBin(path + binName)
}

func main() {
	execute := make(map[string]func([]byte) []byte)
	execute["golang"] = executeGo
	execute["rust"] = executeRust

	assets := http.StripPrefix("/", http.FileServer(http.Dir("assets/")))
	http.Handle("/", assets)
	http.HandleFunc("/execute", func(w http.ResponseWriter, r *http.Request) { // expecting /execute?lang=
		defer r.Body.Close()
		src, _ := ioutil.ReadAll(r.Body)
		lang, exists := r.URL.Query()["lang"]
		if exists {
			f, exists := execute[lang[0]]
			if exists {
				output := f(src)
				w.Write([]byte(strings.Replace(string(output), "\n", "<br>", -1)))
			}
		}
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
